package princess.shoppinglist;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by drippler on 7/18/17.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {

    private LinkedList<ItemDTO> data;

    public ItemAdapter() {
        this.data = new LinkedList<>();
        // TODO add stable ids (maybe sparse array, the insertion order will be the id)
        // https://stackoverflow.com/questions/33012391/how-to-remain-at-a-scroll-position-in-recyclerview-after-adding-items-at-its-fir
        //setHasStableIds(true);
    }

    public void insert(ItemDTO itemDTO) {
        this.data.addFirst(itemDTO);
        notifyItemInserted(0);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemView view = new ItemView(parent.getContext());
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.getItemView().setItem(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        ItemViewHolder(ItemView view) {
            super(view);
        }

        ItemView getItemView() {
            return (ItemView) itemView;
        }
    }

}
