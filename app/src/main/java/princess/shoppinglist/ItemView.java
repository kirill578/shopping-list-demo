package princess.shoppinglist;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by drippler on 7/18/17.
 */

public class ItemView extends LinearLayout {

    private TextView nameTextView;
    private TextView weightTextView;
    private ImageView bagImageView;

    public ItemView(Context context) {
        super(context);
        init();
    }

    public ItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public ItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init() {
        setOrientation(HORIZONTAL);
        int pedding = getResources().getDimensionPixelOffset(R.dimen.item_padding);
        setPadding(pedding, pedding, pedding, pedding);

        View.inflate(getContext(), R.layout.item, this);

        nameTextView = ((TextView) findViewById(R.id.item__name));
        weightTextView = ((TextView) findViewById(R.id.item__weight));
        bagImageView = ((ImageView) findViewById(R.id.item__bag_image));
    }


    public void setItem(ItemDTO itemDTO) {
        nameTextView.setText(itemDTO.getName());
        weightTextView.setText(itemDTO.getWeight());

        int color = Color.parseColor(itemDTO.getBagColor());
        Drawable drawable = bagImageView.getDrawable();
        drawable.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.MULTIPLY));
    }
}
