package princess.shoppinglist;

import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Window;

import com.google.gson.Gson;

import okhttp3.WebSocket;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocketListener;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    // TODO:
    // * add parallax animation when adding item
    // * screen rotation
    // * stable ids improve performance

    private RecyclerView recyclerView;
    private ItemAdapter adapter;
    private Gson gson = new Gson();
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new ItemAdapter();
        recyclerView.setAdapter(adapter);

        setupSocket();
    }

    @MainThread
    private void onNewItemDTO(ItemDTO itemDTO) {
        adapter.insert(itemDTO);
        if (layoutManager.findFirstVisibleItemPosition() == 0)
            recyclerView.scrollToPosition(0);
    }

    void setupSocket() {
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(0,  TimeUnit.MILLISECONDS)
                .build();

        Request request = new Request.Builder()
                .url(getString(R.string.webscoket_url))
                .build();

        client.newWebSocket(request, new WebSocketListener() {
            @Override
            public void onMessage(WebSocket webSocket, String text) {
                final ItemDTO itemDTO = gson.fromJson(text, ItemDTO.class);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onNewItemDTO(itemDTO);
                    }
                });
            }
        });
    }

}