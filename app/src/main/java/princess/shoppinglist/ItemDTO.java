package princess.shoppinglist;

/**
 * Created by drippler on 7/18/17.
 */

public class ItemDTO {

    private String name;
    private String weight;
    private String bagColor;


    public ItemDTO(String name, String weight, String bagColor) {
        this.name = name;
        this.weight = weight;
        this.bagColor = bagColor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBagColor() {
        return bagColor;
    }

    public void setBagColor(String bagColor) {
        this.bagColor = bagColor;
    }
}
